var spinnerTime = 1000;

function showSpinner () {
	$("#spinner").removeClass("hidden")
}

function hideSpinner () {
	$("#spinner").addClass("hidden")
}

$("#option_1").click(function () {
	showSpinner();
	setTimeout(function() {
		$("#crear").addClass("hidden")
		$("#content_1").removeClass("hidden")
		resetItem();
		hideSpinner();
	}, spinnerTime);
})


$("#back_1").click(function () {
	showSpinner();
	setTimeout(function() {
		$("#back_1").parent().addClass("hidden")
		$("#crear").removeClass("hidden")
		hideSpinner();
	}, spinnerTime);
})

function color (color) {
	showSpinner();
	setTimeout(function() {
		$("#images").find(".active").removeClass("active")
		$(".colors").find(".active").removeClass("active")
		$(".colors ."+color).addClass("active")
		$("#images img."+color).addClass("active")
		hideSpinner();
	}, spinnerTime);
}

function resetItem () {
	$("#images").find(".active").removeClass("active")
	$("#images img.blank").addClass("active")
	$(".colors").find(".active").removeClass("active")
	$(".colors .blank").addClass("active")
}

function configure () {
	alert("Configuracion Pendiente");
}
